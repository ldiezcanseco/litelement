import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';


class PersonaApp extends LitElement {

	static get properties(){
		return {
			
		};
	}
	
	constructor(){
		super();
	}


    render() {
        return html `
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
		
            <persona-header></persona-header>
			
			<div class="row">
				<persona-sidebar  @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
				<persona-main class="col-10"></persona-main>
			</div>
			
			<persona-footer></persona-footer>
        `;
    }
	
	newPerson(e){
		console.log("newPerson en PersonaApp");
		this.shadowRoot.querySelector("persona-main").showPersonForm = true;
	}
}
customElements.define('persona-app', PersonaApp)